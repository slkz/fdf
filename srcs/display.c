/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:07:49 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:07:53 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	set_display(t_env *env)
{
	if (env->map->y_max == 1 && env->map->x_max[0] == 0)
		exit(EXIT_FAILURE);
	create_map_array(env);
	get_iso_coord(env);
}

int		is_drawable(t_env *env, ssize_t x, ssize_t y)
{
	return (x < (ssize_t)env->width && x > 0
	&& y < (ssize_t)env->height && y > 0);
}

void	draw_points(t_env *env)
{
	t_map	*map;
	size_t	i;
	size_t	j;

	map = env->map;
	i = 0;
	while (i < map->y_max)
	{
		j = 0;
		while (j < map->x_max[i])
		{
			if (is_drawable(env, map->pixels[i][j].x, map->pixels[i][j].y))
				mlx_pixel_put_img(map->imginfos, map->pixels[i][j].x,
				map->pixels[i][j].y, map->pixels[i][j].color);
			j++;
		}
		i++;
	}
}

void	draw_grid(t_env *env)
{
	t_map	*map;
	size_t	i;
	size_t	j;

	i = 0;
	map = env->map;
	while (i < map->y_max - 1)
	{
		j = 0;
		while (j < map->x_max[i] - 1)
		{
			mlx_drawline(env, &(map->pixels[i][j]), &(map->pixels[i][j + 1]));
			mlx_drawline(env, &(map->pixels[i][j]), &(map->pixels[i + 1][j]));
			j++;
		}
		if (j <= map->x_max[i + 1] - 1)
			mlx_drawline(env, &(map->pixels[i][j]), &(map->pixels[i + 1][j]));
		i++;
	}
	j = 0;
	while (j < map->x_max[i] - 1)
	{
		mlx_drawline(env, &(map->pixels[i][j]), &(map->pixels[i][j + 1]));
		j++;
	}
}
