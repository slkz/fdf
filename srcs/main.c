/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:08:17 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:08:19 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	del(void *content, size_t content_size)
{
	ft_bzero(content, content_size);
	free(content);
}

void	mlx_clear_img(t_imginfo *p_imginfo, int width, int height)
{
	size_t		size;

	size = width * (p_imginfo->bpp / 8) + height * p_imginfo->size_line;
	ft_bzero(p_imginfo->data, size);
}

int		main(int ac, char *av[])
{
	t_map			map;
	t_imginfo		imginfos;
	t_env			env;

	if (ac != 2)
		print_usage();
	init_map(&map);
	parse(av[1], &map);
	if ((env.p_mlx = mlx_init()) == NULL)
		return (-1);
	env.width = 1642;
	env.height = 1242;
	env.p_img = mlx_init_image(env.p_mlx, &imginfos, 1642, 1642);
	env.p_win = mlx_new_window(env.p_mlx, 1642, 1242, "fdf");
	env.map = &map;
	env.iso_flag = 1;
	env.draw_flag = 1;
	(env.map)->imginfos = &imginfos;
	set_display(&env);
	mlx_do_key_autorepeaton(env.p_mlx);
	mlx_hook(env.p_win, 2, (1L << 0), key_hook, &env);
	mlx_mouse_hook(env.p_win, mouse_hook, &env);
	mlx_expose_hook(env.p_win, expose_hook, &env);
	mlx_loop(env.p_mlx);
	return (0);
}
