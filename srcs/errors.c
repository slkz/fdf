/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:08:05 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:08:07 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	print_maperror(char *mapfile)
{
	ft_putstr_fd("[-] Error:\n", 2);
	ft_putstr_fd("    fdf: ", 2);
	perror(mapfile);
	exit(EXIT_FAILURE);
}

void	print_usage(void)
{
	ft_putstr_fd("[-] Error:\n", 2);
	ft_putstr_fd("    fdf: usage: ./fdf <map_file>\n", 2);
	exit(EXIT_FAILURE);
}
