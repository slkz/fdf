/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   coords.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:07:38 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:07:39 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	positive_coords(t_env *env)
{
	t_map		*map;
	size_t		i;
	size_t		j;

	i = 0;
	map = env->map;
	map->x_pad = 821 - (map->x_max_iso - map->x_min_iso) / 2 - map->x_mov;
	map->y_pad = 621 - (map->y_max_iso - map->y_min_iso) / 2 - map->y_mov;
	while (i < map->y_max)
	{
		j = 0;
		while (j < map->x_max[i])
		{
			map->pixels[i][j].x += (map->x_pad - map->x_min_iso);
			map->pixels[i][j].y += (map->y_pad - map->y_min_iso);
			j++;
		}
		i++;
	}
}

void	do_the_maths(t_env *env, size_t i, size_t j)
{
	double	c[3];
	double	s[3];
	int		z;
	t_map	*map;

	map = env->map;
	c[0] = cos(map->x_ang);
	c[1] = cos(map->y_ang);
	c[2] = cos(map->z_ang);
	s[0] = sin(map->x_ang);
	s[1] = sin(map->y_ang);
	s[2] = sin(map->z_ang);
	z = map->coord[i][j];
	(map->pixels[i][j]).x = (c[2] * (c[1] * j + s[1] * (s[0] * i - c[0] * z))
			- s[2] * (c[0] * i - s[0] * z) * map->zoom)
			+ (map->z_scale * (s[1] * j - c[1] * (s[0] * i - c[0] * z)));
	(map->pixels[i][j]).y = (s[2] * (c[1] * j + s[1] * (s[0] * i - c[0] * z))
			+ c[2] * (c[0] * i - s[0] * z) * map->zoom)
			+ (map->z_scale * (s[1] * j - c[1] * (s[0] * i - c[0] * z)));
}

void	check_minmax(t_env *env, size_t i, size_t j)
{
	t_map	*map;

	map = env->map;
	if ((map->pixels[i][j]).x < (ssize_t)map->x_min_iso)
		map->x_min_iso = map->pixels[i][j].x;
	if ((map->pixels[i][j]).x > (ssize_t)map->x_max_iso)
		map->x_max_iso = map->pixels[i][j].x;
	if ((map->pixels[i][j]).y < (ssize_t)map->y_min_iso)
		map->y_min_iso = map->pixels[i][j].y;
	if ((map->pixels[i][j]).y > (ssize_t)map->y_max_iso)
		map->y_max_iso = map->pixels[i][j].y;
}

void	update_iso_coord(t_env *env)
{
	t_map	*map;
	size_t	i;
	size_t	j;

	map = env->map;
	i = 0;
	reset_map(map);
	while (i < map->y_max)
	{
		j = 0;
		while (j < map->x_max[i])
		{
			do_the_maths(env, i, j);
			check_minmax(env, i, j);
			j++;
		}
		i++;
	}
	positive_coords(env);
}

void	get_iso_coord(t_env *env)
{
	t_map	*map;
	size_t	i;
	size_t	j;

	map = env->map;
	i = 0;
	reset_map(map);
	while (i < map->y_max)
	{
		j = 0;
		while (j < map->x_max[i])
		{
			(map->pixels[i][j]).x = (0.71 * j - 0.71 * i) * map->zoom;
			(map->pixels[i][j]).y = (0.41 * i + 0.41 * j) * map->zoom
				- (0.82 * map->coord[i][j] * map->z_scale);
			(map->pixels[i][j]).z = map->coord[i][j];
			(map->pixels[i][j]).color = get_point_color(map->pixels[i][j].z);
			check_minmax(env, i, j);
			j++;
		}
		i++;
	}
	positive_coords(env);
}
