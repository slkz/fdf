/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_drawline.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:08:29 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:08:30 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	dx_supzero(t_env *env, t_pix *pix_a, t_pix *pix_b)
{
	int		dx;
	int		dy;

	if ((dy = pix_b->y - pix_a->y) != 0)
	{
		if (dy > 0)
		{
			if ((dx = pix_b->x - pix_a->x) >= dy)
				dx_sup_dy_pos(env, pix_a, pix_b);
			else
				dx_inf_dy_pos(env, pix_a, pix_b);
		}
		else
		{
			if ((dx = pix_b->x - pix_a->x) >= -dy)
				dx_sup_dy_neg(env, pix_a, pix_b);
			else
				dx_inf_dy_neg(env, pix_a, pix_b);
		}
	}
	else
		dy_equ_zero(env, pix_a, pix_b);
}

void	dx_equ_zero(t_env *env, t_pix *pix_a, t_pix *pix_b)
{
	int		dy;
	t_pix	cur;

	cur.x = pix_a->x;
	cur.y = pix_a->y;
	if ((dy = pix_b->y - cur.y) > 0)
	{
		while (cur.y != pix_b->y)
		{
			if (is_drawable(env, cur.x, cur.y))
				mlx_pixel_put_img(env->map->imginfos, cur.x, cur.y,
					get_gradient_color(pix_a, pix_b, cur.x, cur.y));
			cur.y++;
		}
	}
	else if ((dy = pix_b->y - cur.y) <= 0)
	{
		while (cur.y != pix_b->y)
		{
			if (is_drawable(env, cur.x, cur.y))
				mlx_pixel_put_img(env->map->imginfos, cur.x, cur.y,
					get_gradient_color(pix_a, pix_b, cur.x, cur.y));
			cur.y--;
		}
	}
}

void	mlx_drawline(t_env *env, t_pix *pix_a, t_pix *pix_b)
{
	int			dx;
	t_pix		*begin;
	t_pix		*end;

	begin = (pix_a->x < pix_b->x) ? pix_a : pix_b;
	end = (pix_a->x < pix_b->x) ? pix_b : pix_a;
	if ((dx = end->x - begin->x) != 0)
		dx_supzero(env, begin, end);
	else
		dx_equ_zero(env, begin, end);
}
