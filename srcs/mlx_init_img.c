/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_init_img.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:08:38 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:08:46 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "mlx_types.h"

void	*mlx_init_image(void *mlx_ptr, t_imginfo *p_inf, int width, int height)
{
	void	*img;

	img = mlx_new_image(mlx_ptr, width, height);
	p_inf->data = mlx_get_data_addr(img, &(p_inf->bpp), &(p_inf->size_line),
			&(p_inf->endian));
	return (img);
}
