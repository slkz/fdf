/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_put_pixel_img.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:08:53 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:08:55 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mlx_types.h"

void		mlx_pixel_put_img(t_imginfo *p_imginfo, int x, int y,
	unsigned int color)
{
	int		i;

	i = x * (p_imginfo->bpp / 8) + y * p_imginfo->size_line;
	(p_imginfo->data)[i] = color;
	(p_imginfo->data)[i + 1] = color >> 8;
	(p_imginfo->data)[i + 2] = color >> 16;
}
