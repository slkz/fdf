/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:08:11 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:12:16 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		expose_hook(t_env *env)
{
	env->map->x_min = 0;
	env->map->y_min = 0;
	mlx_clear_img(env->map->imginfos, 1642, 1242);
	update_iso_coord(env);
	if (env->iso_flag == 1)
		get_iso_coord(env);
	draw_points(env);
	if (env->draw_flag == 1)
		draw_grid(env);
	mlx_put_image_to_window(env->p_mlx, env->p_win, env->p_img, 0, 0);
	return (0);
}

int		mouse_hook(int button, int x, int y, t_env *env)
{
	(void)x;
	(void)y;
	if (button == 4)
		env->map->z_scale += 1;
	if (button == 5)
		env->map->z_scale -= 1;
	expose_hook(env);
	return (0);
}

void	rotate_hook(int keycode, t_env *env)
{
	if (keycode == 91)
		env->map->x_ang += 0.05;
	if (keycode == 84)
		env->map->x_ang -= 0.05;
	if (keycode == 88)
		env->map->z_ang += 0.05;
	if (keycode == 86)
		env->map->z_ang -= 0.05;
	if (keycode == 89)
		env->map->y_ang -= 5;
	if (keycode == 92)
		env->map->y_ang += 5;
	if (keycode == 75)
		env->iso_flag ^= 1;
	if (keycode == 67)
		env->draw_flag ^= 1;
}

void	zoom_move(int keycode, t_env *env)
{
	if (keycode == 69)
		env->map->zoom += 1;
	if (keycode == 78)
		env->map->zoom -= 1;
	if (keycode == 116)
		env->map->z_scale += 1;
	if (keycode == 121)
		env->map->z_scale -= 1;
	if (keycode == 125)
		env->map->y_mov += 3;
	if (keycode == 126)
		env->map->y_mov -= 3;
	if (keycode == 124)
		env->map->x_mov += 3;
	if (keycode == 123)
		env->map->x_mov -= 3;
}

int		key_hook(int keycode, t_env *env)
{
	if (keycode == 53)
	{
		mlx_destroy_image(env->p_mlx, env->p_img);
		mlx_destroy_window(env->p_mlx, env->p_win);
		destroy_map(env->map);
		exit(0);
	}
	zoom_move(keycode, env);
	rotate_hook(keycode, env);
	if (keycode == 71)
	{
		env->map->y_ang = 0.00;
		env->map->y_ang = 0.00;
		env->map->y_ang = 0.00;
		env->iso_flag = 1;
		reset_map(env->map);
	}
	expose_hook(env);
	return (0);
}
