/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:09:00 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:09:02 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

size_t		index_list(const t_list *list, void **tab, size_t size)
{
	t_list	*ptr;
	size_t	i;

	i = 0;
	if ((ptr = (t_list *)list) == NULL)
		return (0);
	while (ptr != NULL)
	{
		ft_memcpy((unsigned char*)*tab + i * size,
			(unsigned char *)(ptr->content),
			size);
		ptr = ptr->next;
		i++;
	}
	return (i);
}

ssize_t		parse_line_to_list(const char *line, t_list **list)
{
	ssize_t		z;
	size_t		x;
	int			i;

	x = 0;
	i = 0;
	while (line[i] != '\0')
	{
		if (line[i] != ' ' && line[i] != '\t')
		{
			x++;
			z = ft_atoi(line + i);
			ft_lstpushback(list, &z, sizeof(ssize_t));
			while (line[i] != ' ' && line[i] != '\0' && line[i] != '\t')
				i++;
			if (line[i] == '\0')
				break ;
		}
		i++;
	}
	return (x);
}

t_line		*parse_line(const char *line)
{
	t_list	*list;
	t_line	*node;
	ssize_t	x;

	x = 0;
	(void)x;
	list = NULL;
	if ((node = (t_line *)malloc(sizeof(t_line))) == NULL)
		return (NULL);
	x = parse_line_to_list(line, &list);
	if ((node->tab = (int *)malloc(sizeof(int) * x + 1)) == NULL)
		return (NULL);
	node->size = index_list(list, (void **)&(node->tab), sizeof(int));
	ft_lstdel(&list, del);
	return (node);
}

void		map_file(t_list *lines, t_map *map)
{
	t_list		*ptr_lines;
	int			i;

	if ((ptr_lines = lines) == NULL)
		return ;
	i = 0;
	while (ptr_lines != NULL)
	{
		map->coord[i] = ((t_line *)(ptr_lines->content))->tab;
		map->x_max[i] = ((t_line *)(ptr_lines->content))->size;
		ptr_lines = ptr_lines->next;
		i++;
	}
	map->lines = lines;
}

void		parse(char *mapfile, t_map *map)
{
	t_list	*lines;
	t_line	*s_line;
	char	*line;
	int		fd;

	lines = NULL;
	line = NULL;
	if ((fd = open(mapfile, O_RDONLY)) == -1)
		print_maperror(mapfile);
	while (get_next_line(fd, &line) > 0)
	{
		s_line = parse_line(line);
		ft_lstpushback(&lines, s_line, sizeof(t_line));
		map->y_max++;
		free(s_line);
		free(line);
	}
	if ((map->coord = (int **)malloc(sizeof(int *) * map->y_max)) == NULL
		|| (map->x_max = (size_t *)malloc(sizeof(size_t) * map->y_max)) == NULL
		|| lines == NULL)
		exit(EXIT_FAILURE);
	map_file(lines, map);
	close(fd);
}
