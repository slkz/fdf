/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:07:26 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:07:29 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

unsigned int	get_point_color(int z)
{
	if (z <= VERY_DEEP)
		return (0x2614E3);
	else if (z <= DEEP && z > VERY_DEEP)
		return (0x4489E3);
	else if (z <= NORMAL && z > DEEP)
		return (0x15C3EB);
	else if (z <= TALL && z > NORMAL)
		return (0x41BF28);
	else if (z <= HIGH && z > TALL)
		return (0xDAE024);
	else if (z <= VERY_HIGH && z > HIGH)
		return (0xff0000);
	else
		return (0x42ef48);
}

int				get_segment_percent(t_pix *a, t_pix *b, int x, int y)
{
	int		dy;
	int		dx;
	double	length;
	double	segment_length;

	dy = b->y - a->y;
	dx = b->x - a->x;
	length = sqrt(pow(dx, 2) + pow(dy, 2));
	dx = x - a->x;
	dy = y - a->y;
	segment_length = sqrt(pow(dx, 2) + pow(dy, 2));
	return ((int)(segment_length / length * 100));
}

unsigned int	get_gradient_color(t_pix *a, t_pix *b, int x, int y)
{
	t_rgb	curr_rgb;
	t_rgb	base_rgb;
	t_rgb	dest_rgb;
	int		color;
	int		percent;

	percent = get_segment_percent(a, b, x, y);
	base_rgb.r = (a->color >> 16) & 0xFF;
	base_rgb.g = (a->color >> 8) & 0xFF;
	base_rgb.b = (a->color) & 0xFF;
	dest_rgb.r = (b->color >> 16) & 0xFF;
	dest_rgb.g = (b->color >> 8) & 0xFF;
	dest_rgb.b = (b->color) & 0xFF;
	curr_rgb.r = base_rgb.r + percent / 100.0 * (dest_rgb.r - base_rgb.r);
	curr_rgb.g = base_rgb.g + percent / 100.0 * (dest_rgb.g - base_rgb.g);
	curr_rgb.b = base_rgb.b + percent / 100.0 * (dest_rgb.b - base_rgb.b);
	color = ((unsigned int)curr_rgb.r & 0xff) << 16
		| ((unsigned int)curr_rgb.g & 0xff) << 8
		| ((unsigned int)curr_rgb.b & 0xff);
	return (color);
}
