/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_vertical_line.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:07:58 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:07:59 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	dx_sup_dy_pos(t_env *env, t_pix *pix_a, t_pix *pix_b)
{
	int		e;
	int		dx;
	int		dy;
	t_pix	cur;

	cur.x = pix_a->x;
	cur.y = pix_a->y;
	e = (pix_b->x - cur.x);
	dy = (pix_b->y - cur.y) * 2;
	dx = e * 2;
	while (cur.x != pix_b->x)
	{
		if (is_drawable(env, cur.x, cur.y))
		{
			mlx_pixel_put_img(env->map->imginfos, cur.x, cur.y,
				get_gradient_color(pix_a, pix_b, cur.x, cur.y));
		}
		if ((e -= dy) < 0 && cur.y++)
			e += dx;
		cur.x++;
	}
}

void	dx_inf_dy_pos(t_env *env, t_pix *pix_a, t_pix *pix_b)
{
	int		e;
	int		dx;
	int		dy;
	t_pix	cur;

	cur.x = pix_a->x;
	cur.y = pix_a->y;
	e = (pix_b->y - cur.y);
	dx = (pix_b->x - cur.x) * 2;
	dy = e * 2;
	while (cur.y != pix_b->y)
	{
		if (is_drawable(env, cur.x, cur.y))
			mlx_pixel_put_img(env->map->imginfos, cur.x, cur.y,
				get_gradient_color(pix_a, pix_b, cur.x, cur.y));
		if ((e -= dx) < 0 && cur.x++)
			e += dy;
		cur.y++;
	}
}

void	dx_sup_dy_neg(t_env *env, t_pix *pix_a, t_pix *pix_b)
{
	int		e;
	int		dx;
	int		dy;
	t_pix	cur;

	cur.x = pix_a->x;
	cur.y = pix_a->y;
	e = (pix_b->x - cur.x);
	dy = (pix_b->y - cur.y) * 2;
	dx = e * 2;
	while (cur.x != pix_b->x)
	{
		if (is_drawable(env, cur.x, cur.y))
			mlx_pixel_put_img(env->map->imginfos, cur.x, cur.y,
				get_gradient_color(pix_a, pix_b, cur.x, cur.y));
		if ((e += dy) < 0 && cur.y--)
			e += dx;
		(cur.x)++;
	}
}

void	dx_inf_dy_neg(t_env *env, t_pix *pix_a, t_pix *pix_b)
{
	int		e;
	int		dx;
	int		dy;
	t_pix	cur;

	cur.x = pix_a->x;
	cur.y = pix_a->y;
	e = (pix_b->y - cur.y);
	dx = (pix_b->x - cur.x) * 2;
	dy = e * 2;
	while (cur.y != pix_b->y)
	{
		if (is_drawable(env, cur.x, cur.y))
			mlx_pixel_put_img(env->map->imginfos, cur.x, cur.y,
				get_gradient_color(pix_a, pix_b, cur.x, cur.y));
		if ((e += dx) > 0 && cur.x++)
			e += dy;
		cur.y--;
	}
}

void	dy_equ_zero(t_env *env, t_pix *pix_a, t_pix *pix_b)
{
	t_pix cur;

	cur.x = pix_a->x;
	cur.y = pix_a->y;
	while (cur.x != pix_b->x)
	{
		if (is_drawable(env, cur.x, cur.y))
			mlx_pixel_put_img(env->map->imginfos, cur.x, cur.y,
				get_gradient_color(pix_a, pix_b, cur.x, cur.y));
		cur.x++;
	}
}
