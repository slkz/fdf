/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:08:23 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:08:25 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	init_map(t_map *map)
{
	map->coord = NULL;
	map->pixels = NULL;
	map->imginfos = NULL;
	map->x_min = 0;
	map->y_min = 0;
	map->x_max = NULL;
	map->y_max = 0;
	map->x_min_iso = 0;
	map->y_min_iso = 0;
	map->x_max_iso = 0;
	map->y_max_iso = 0;
	map->zoom = 16;
	map->z_scale = 1;
	map->x_mov = 0;
	map->y_mov = 0;
	map->x_pad = 0;
	map->y_pad = 0;
	map->x_ang = 0;
	map->y_ang = 0;
	map->z_ang = 0;
}

void	destroy_map(t_map *map)
{
	size_t	i;

	i = 0;
	while (i < map->y_max)
	{
		ft_bzero(map->coord[i], map->x_max[i]);
		ft_bzero(map->pixels[i], map->x_max[i]);
		free(map->coord[i]);
		free(map->pixels[i]);
		i++;
	}
	ft_bzero(map->x_max, map->y_max);
	free(map->x_max);
	free(map->coord);
	free(map->pixels);
	ft_lstdel(&(map->lines), del);
}

void	create_map_array(t_env *env)
{
	size_t	i;
	t_map	*map;

	i = 0;
	map = env->map;
	if ((map->pixels = malloc(sizeof(t_pix *) * map->y_max)) == NULL)
		exit(EXIT_FAILURE);
	while (i < map->y_max)
	{
		if ((map->pixels[i] = malloc(sizeof(t_pix) * map->x_max[i])) == NULL)
			exit(EXIT_FAILURE);
		i++;
	}
}

void	reset_map(t_map *map)
{
	map->x_min_iso = 0;
	map->y_min_iso = 0;
	map->x_max_iso = 0;
	map->y_max_iso = 0;
}
