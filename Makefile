#--------------------------------- Makefile -----------------------------------#

# Executable name
NAME = fdf 
  
# Project layout
SRC_DIR = ./srcs/
INC_DIR = ./includes/
OBJ_DIR = ./obj/

# Compiler and linker settings
CC = gcc
CFLAGS = -Werror -Wall -Wextra -O3 -g3
LFLAGS = -framework OpenGL -framework AppKit -lmlx -L./libft/ -lft
IFLAGS = -I$(INC_DIR) -I./libft/
  
# Extra libraries
LIBFT = libft/libft.a

# Project files
SRC_FILES = main.c errors.c hooks.c mlx_init_img.c mlx_put_pixel_img.c \
			parser.c map.c coords.c display.c colors.c mlx_drawline.c \
			draw_vertical_line.c

OBJ_FILES = $(SRC_FILES:.c=.o)
INC_FILES = fdf.h mlx_types.h 

# List of files
SRC = $(addprefix $(SRC_DIR), $(SRC_FILES))
OBJ = $(addprefix $(OBJ_DIR), $(OBJ_FILES))
INC = $(addprefix $(INC_DIR), $(INC_FILES))

.PHONY: all clean fclean re

#------------------------------------------------------------------------------#

all: $(OBJ_DIR) $(LIBFT) $(NAME) auteur

$(OBJ_DIR):
	@mkdir -p $(OBJ_DIR)

auteur:
	$(shell echo 'lucuzzuc' > $@)

$(NAME): $(OBJ)
	$(CC) -o $@ $^ $(LFLAGS)

$(LIBFT):
	make -C libft/

$(OBJ_DIR)%.o: $(SRC_DIR)%.c $(INC)
	$(CC) $(CFLAGS) $(IFLAGS) -o $@ -c $<

clean:
	make -C libft/ clean
	rm -rf $(OBJ_DIR)

fclean: clean
	rm -f $(NAME) $(LIBFT)

re: fclean all
