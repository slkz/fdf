/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_types.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:12:53 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:12:54 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MLX_TYPES_H
# define MLX_TYPES_H

# include <mlx.h>
# include <unistd.h>
# include "libft.h"

# define VERY_DEEP	-7
# define DEEP		-3
# define NORMAL		0
# define TALL		2
# define HIGH		5
# define VERY_HIGH	7

typedef struct		s_rgb
{
	float		r;
	float		g;
	float		b;
}					t_rgb;

typedef	struct		s_line
{
	int				*tab;
	size_t			size;
}					t_line;

typedef struct		s_pix {
	ssize_t			x;
	ssize_t			y;
	ssize_t			z;
	unsigned int	color;
}					t_pix;

typedef struct		s_imginfo {
	char			*data;
	int				bpp;
	int				size_line;
	int				endian;
}					t_imginfo;

typedef struct		s_map {
	t_list			*lines;
	int				**coord;
	t_pix			**pixels;
	t_imginfo		*imginfos;
	int				x_min;
	int				y_min;
	size_t			*x_max;
	size_t			y_max;
	size_t			x_min_iso;
	size_t			y_min_iso;
	size_t			x_max_iso;
	size_t			y_max_iso;
	ssize_t			zoom;
	ssize_t			z_scale;
	size_t			x_pad;
	size_t			y_pad;
	ssize_t			x_mov;
	ssize_t			y_mov;
	float			x_ang;
	float			y_ang;
	float			z_ang;
}					t_map;

typedef struct		s_env {
	t_map			*map;
	void			*p_mlx;
	void			*p_img;
	void			*p_win;
	size_t			width;
	size_t			height;
	int				iso_flag;
	int				draw_flag;
}					t_env;

#endif
