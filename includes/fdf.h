/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/21 19:12:46 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:12:48 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <mlx.h>
# include "mlx_types.h"
# include <stdlib.h>
# include <stdio.h>
# include <fcntl.h>
# include <math.h>
# include "libft.h"

/*
**	Functions prototypes.
**	---------------------
*/

/*
**	Parser functions: parser.c
**	--------------------------
**		Parse map file to memory using an indexed linked-list
*/

void			del(void *content, size_t content_size);
void			parse(char *mapfile, t_map *map);
void			map_file(t_list *lines, t_map *map);
t_line			*parse_line(const char *line);
ssize_t			parse_line_to_list(const char *line, t_list **list);
size_t			index_list(const t_list *list, void **tab, size_t size);

/*
**	Map functions: map.c
**	--------------------
**		Allocate and initialize map, show it, destroy it
*/

void			init_map(t_map *map);
void			destroy_map(t_map *map);
void			create_map_array(t_env *env);
void			reset_map(t_map *map);
void			show_coord(t_map *map);

/*
**	Events handling: hooks.c
**	------------------------
**		Various actions upon keypress, mouse click or scroll
*/

int				mouse_hook(int button, int x, int y, t_env *env);
int				expose_hook(t_env *env);
int				key_hook(int keycode, t_env *env);

/*
**	Error handling: errors.c
**	------------------------
**		Abort before any segfault, bus error, fpe and other cool stuff
*/
void			print_usage(void);
void			print_maperror(char *mapfile);

/*
**	mlx-specific functions: move to libft pls
*/
void			mlx_pixel_put_img(t_imginfo *p_imginfo, int x, int y,
		unsigned int color);
void			*mlx_init_image(void *mlx_ptr, t_imginfo *p_info,
		int width, int height);
void			mlx_clear_img(t_imginfo *p_imginfo, int width, int height);

/*
**	Coordinates functions: coords.c
**	-------------------------------
**		Converts 3d -> 2d and update map upon hooks.
*/
void			positive_coords(t_env *env);
void			get_iso_coord(t_env *env);
void			update_iso_coord(t_env *env);

/*
**	Display functions: display.c
**	----------------------------
**		Display points previously computed
*/

void			set_display(t_env *env);
void			draw_grid(t_env *env);
void			draw_points(t_env *env);
int				is_drawable(t_env *env, ssize_t x, ssize_t y);
unsigned int	get_gradient_color(t_pix *a, t_pix *b, int x, int y);
unsigned int	get_point_color(int z);

/*
**  Mlx drawline: mlx_drawline.c
**  ----------------------------
**      Bresenham's algorithm implementation. Works on intergers, perf++ !
*/

void			mlx_drawline(t_env *env, t_pix *pix_a, t_pix *pix_b);
void			dx_equ_zero(t_env *env, t_pix *pix_a, t_pix *pix_b);
void			dx_supzero(t_env *env, t_pix *pix_a, t_pix *pix_b);

/*
**  Vertical lines: draw_vertical_lines.c
**  ----------------------------
**      Part of Bresenham algorithm, handles vertical lines
*/

void			dy_equ_zero(t_env *env, t_pix *pix_a, t_pix *pix_b);
void			dx_inf_dy_neg(t_env *env, t_pix *pix_a, t_pix *pix_b);
void			dx_sup_dy_neg(t_env *env, t_pix *pix_a, t_pix *pix_b);
void			dx_inf_dy_pos(t_env *env, t_pix *pix_a, t_pix *pix_b);
void			dx_sup_dy_pos(t_env *env, t_pix *pix_a, t_pix *pix_b);

#endif
