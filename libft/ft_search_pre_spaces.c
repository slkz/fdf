/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_search_pre_spaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:03:26 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:36 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int	ft_search_pre_spaces(char *str)
{
	unsigned int	i;
	unsigned int	count;

	i = 0;
	count = 0;
	if (ft_isspace(str[i]) == 1)
	{
		while (ft_isspace(str[i]) == 1)
		{
			count++;
			i++;
		}
	}
	return (count);
}
