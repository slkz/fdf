/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:03:04 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:34 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*ptr;
	t_list	*nextlst;

	ptr = *alst;
	while (ptr)
	{
		nextlst = ptr->next;
		ft_lstdelone(&ptr, del);
		ptr = nextlst;
	}
	*alst = NULL;
}
