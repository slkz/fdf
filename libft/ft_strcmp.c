/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:03:30 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:36 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strcmp(const char *s1, const char *s2)
{
	int				i;
	unsigned int	diff;

	i = 0;
	while (s1[i] && s2[i])
	{
		if (s1[i] == s2[i])
			i++;
		else
		{
			diff = (unsigned char)s1[i] - (unsigned char)s2[i];
			return (diff);
		}
	}
	return ((unsigned char)s1[i] - (unsigned char)s2[i]);
}
