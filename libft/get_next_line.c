/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 18:42:48 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/16 06:31:01 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include "libft.h"

char	*get_line(t_list **begin_list, t_list *newline)
{
	t_list	*ptr;
	size_t	size;
	char	*line;

	size = 0;
	if ((ptr = *begin_list) == NULL)
		return (NULL);
	while (ptr && ptr != newline)
	{
		size += ptr->content_size - 1;
		ptr = ptr->next;
	}
	if (ptr)
		size += ptr->content_size - 1;
	if ((line = ft_strnew(size)) == NULL)
		return (NULL);
	ptr = *begin_list;
	while (ptr && ptr != newline)
	{
		line = ft_strcat(line, ptr->content);
		ptr = ptr->next;
	}
	if (ptr)
		line = ft_strcat(line, ptr->content);
	return (line);
}

int		get_next_line(int fd, char **line)
{
	static t_list	*list[MAX_FD] = {0};
	t_list			*newline;
	int				ret;
	char			*buf;
	char			*cut;

	ret = 0;
	if (fd < 0 || fd > MAX_FD || !line || (buf = ft_strnew(BUFF_SIZE)) == NULL)
		return (-1);
	while ((newline = ft_lstchr(&list[fd], '\n')) == NULL
			&& (ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		ft_list_push_back(&list[fd], (void *)buf, ret + 1);
	}
	*line = get_line(&list[fd], newline);
	ft_list_cut_head(&list[fd], newline);
	if (*line && (cut = ft_strchr(*line, '\n')) != NULL)
	{
		*cut++ = '\0';
		*cut ? ft_list_push_front(&list[fd], cut, ft_strlen(cut) + 1) : 0;
	}
	free(buf);
	return ((*line != NULL) ? 1 : ret);
}
