/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:05:13 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:37 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	**ft_strsplit(const char *s, char c)
{
	char	**splitted;
	int		index;
	int		size;

	index = 0;
	size = ft_split_len_by_char((char *)s, c);
	if ((splitted = (char **)malloc(sizeof(char *) * size + 1)) == NULL)
		return (NULL);
	while (size--)
	{
		while (*s == c)
			s++;
		splitted[index] = ft_strsub((char *)s, 0, ft_wordlen((char *)s, 0, c));
		if (splitted[index] == NULL)
			return (NULL);
		s = s + ft_wordlen((char *)s, 0, c);
		index++;
	}
	splitted[index] = NULL;
	return (splitted);
}
