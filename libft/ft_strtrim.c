/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:05:18 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:37 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	int		i;
	int		len;
	char	*s2;

	if (s == NULL)
		return (NULL);
	i = 0;
	len = ft_strlen(s);
	while (ft_isspace(s[len - 1]) != 0)
		len--;
	while (ft_isspace(s[i]) != 0)
	{
		len--;
		i++;
	}
	if (len <= 0)
		len = 0;
	s2 = ft_strsub(s, i, len);
	return (s2);
}
