/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:03:15 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:35 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char		*ddst;
	const char	*ssrc;

	ddst = dst;
	ssrc = src;
	if (src <= dst)
	{
		ssrc = ssrc + (len - 1);
		ddst = ddst + (len - 1);
		while (len--)
			*ddst-- = *ssrc--;
	}
	else
		ft_memcpy(dst, src, len);
	return (dst);
}
