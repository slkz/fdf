/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dec_in_int.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:02:55 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:34 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

unsigned int	ft_dec_in_int(int n)
{
	unsigned int	dec;

	dec = 0;
	if (n == -2147483648)
		n--;
	if (n < 0)
		n = -n;
	if (n < 10)
		return (1);
	while (n > 1)
	{
		n = n / 10;
		dec++;
	}
	if (n == 1)
		dec++;
	return (dec);
}
