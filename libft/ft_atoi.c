/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:02:36 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:46 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *str)
{
	int		sign;
	int		nb;

	sign = 1;
	nb = 0;
	while (*str == '\r' || *str == ' ' || *str == '\t' || *str == '\n'
			|| *str == '\v' || *str == '\f')
		str++;
	if (*str == '-' || *str == '+')
	{
		if (*str == '-')
			sign = -1;
		str++;
	}
	while (ft_isdigit(*str) == 1)
	{
		if ((ft_strcmp("-2147483648", str)) == 0)
			return (nb = -2147483648);
		nb = nb * 10 + *str - '0';
		str++;
	}
	nb *= sign;
	return (nb);
}
