/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:03:07 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:35 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*node;
	t_list	*prev;
	t_list	*head;

	head = NULL;
	while (lst)
	{
		if ((node = (t_list *)malloc(sizeof(t_list))) == NULL)
			return (NULL);
		node->next = NULL;
		node->content_size = lst->content_size;
		node->content = lst->content;
		node = f(node);
		if (head == NULL)
			head = node;
		else
			prev->next = node;
		prev = node;
		lst = lst->next;
	}
	return (head);
}
