/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_front.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/19 15:21:51 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/21 19:09:25 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_list_push_front(t_list **begin_list, void *content,
		size_t content_size)
{
	t_list		*to_push;

	if (!(to_push = ft_lstnew(content, content_size)))
		return ;
	if (*begin_list == NULL)
		*begin_list = to_push;
	else
	{
		to_push->next = *begin_list;
		*begin_list = to_push;
	}
}
