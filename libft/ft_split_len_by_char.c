/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_len_by_char.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:03:26 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:36 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_split_len_by_char(char *s, char c)
{
	int		i;
	int		splitted;

	if (!s || !c)
		return (0);
	i = 0;
	splitted = 0;
	while (s[i])
	{
		if (s[i] == c)
			splitted--;
		while (s[i] && s[i] != c)
			i++;
		splitted++;
		while (s[i] == c)
			i++;
	}
	return (splitted);
}
