/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstpushback.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/16 06:26:44 by lucuzzuc          #+#    #+#             */
/*   Updated: 2018/03/16 06:28:27 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstpushback(t_list **head, void *data, size_t size)
{
	t_list	*node;
	t_list	*ptr;

	if (head == NULL || data == NULL || size == 0
		|| (node = ft_lstnew(data, size)) == NULL)
		return ;
	if (*head == NULL)
	{
		*head = node;
		return ;
	}
	ptr = *head;
	while (ptr->next != NULL)
		ptr = ptr->next;
	ptr->next = node;
}
